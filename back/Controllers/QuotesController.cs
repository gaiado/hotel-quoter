﻿using back.Models.Entities;
using back.Models;
using Microsoft.AspNetCore.Mvc;
using back.Services;

namespace back.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuotesController : ControllerBase
    {

        private readonly IPriceTravelApiService _priceTravelApiService;

        public QuotesController(IPriceTravelApiService priceTravelApiService)
        {
            _priceTravelApiService = priceTravelApiService;
        }

        // POST 
        [HttpPost("Hotels/Rates")]
        public async Task<string> HotelesRates(HotelRateRequest hotelRateRequest)
        {
            string response = await _priceTravelApiService.GetHotelRates(hotelRateRequest);
            return response;
        }
    }
}
