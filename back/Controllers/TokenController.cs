﻿using back.Models;
using back.Models.Entities;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace back.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokensController : ControllerBase
    {
        private readonly IPriceTravelApiService _tokenApiService;

        public TokensController(IPriceTravelApiService tokenApiService)
        {
            _tokenApiService = tokenApiService;
        }
        // GET api/<TokensController>/5
        [HttpGet("Generate/{channelId}")]
        public async Task<TokenResponse> Generate(int channelId)
        {
            TokenResponse tokenResponse = await _tokenApiService.GetToken(channelId);
            return tokenResponse;
        }
    }
}
