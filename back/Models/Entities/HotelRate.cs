﻿
namespace back.Models.Entities
{
    public class HotelRate
    {
        public string hotel { get; set; }
        public Hotel[] hotels { get; set; }
        public string eventId { get; set; }
        public string error { get; set; }


    }
    public class Hotel
    {
        public Room[] rooms { get; set; }
        public int id { get; set; }
        public string name { get; set; }

    }
    public class Room
    {
        public int id { get; set; }
        public string name { get; set; }
        public Rate[] rates { get; set; }
    }
    public class Rate
    {
        public string rateKey { get; set; }
        public string contractId { get; set; }
        public string clientCurrency { get; set; }
        public CancellationPolicy[] cancellationPolicies { get; set; }
        public ReservationPolicy[] reservationPolicies { get; set; }
        public string amount { get; set; }
        public string publicAmount { get; set; }
        public Taxes taxes { get; set; }
        public FeesNotIncluded feesNotIncluded { get; set; }
    }
    public class CancellationPolicy
    {
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        //revisar el amount para cambiar a number o dejar en string
        public string amount { get; set; }
        public string currency { get; set; }
    }
    public class ReservationPolicy
    {
        public string type { get; set; }
        public string code { get; set; }
        public string description { get; set; }
    }
    public class Taxes
    {
        //REVISAR SI HACE FALTA CAMBIAR A NUMERO O DEJAR EN STRING  
        public string total { get; set; }
        public Detail[] details { get; set; }
    }
    public class FeesNotIncluded
    {
        //REVISAR SI HACE FALTA CAMBIAR A NUMERO O DEJAR EN STRING
        public string total { get; set; }
        //REVISAR SI ACEPTA TIPO DETAIL O CAMBIAR A OTRO TIPO DE ARRAY
        public Detail[] details { get; set; }
        public string currency { get; set; }
    }
    public class Detail
    {
        public string type { get; set; }
        //REVISAR SI HACE FALTA CAMBIAR A DOUBLE O DEJAR COMO STRING
        public string number { get; set; }
    }

}




