﻿using Microsoft.EntityFrameworkCore;

namespace back.Models
{
    public class Context : DbContext
    {
        protected readonly IConfiguration _configuration;

        public Context(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            // connect to sqlite database
            options.UseSqlite(_configuration.GetConnectionString("WebApiDatabase"));
        }

        public DbSet<Channel> Channels { get; set; }
    }
}
