﻿using back.Models.Entities;

namespace back.Models
{
    public interface IPriceTravelApiService
    {
        public Task<TokenResponse> GetToken(int channelId);
        public Task<string> GetHotelRates(HotelRateRequest hotelRateRequest);
    }
}
