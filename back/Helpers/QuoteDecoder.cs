﻿using System.Security.Cryptography;
using System.Text;

namespace back.Helpers
{
    public class QuoteDecoder
    {

        private readonly AesManaged _aesManaged;

        public QuoteDecoder()
        {
            var saltBytes = new byte[] { 12, 62, 43, 40, 51, 86, 37, 98 };
            var key = new Rfc2898DeriveBytes("uDyLuGNCtOKRbz2SR5W6js9+uGWJ/OQ0yRsMO4oXoEM=", saltBytes, 1000);
            
            var aesManaged = new AesManaged
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                KeySize = 256,
                Key = key.GetBytes(256 / 8),
            };
            aesManaged.IV = key.GetBytes(aesManaged.BlockSize / 8);

            _aesManaged = aesManaged;
        }

        public string Decode(string plainText)
        {
            using (var ms = new MemoryStream())
            {
                using (var cs = new CryptoStream(ms, _aesManaged.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    var cipherBytes = Convert.FromBase64String(plainText);
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                }

                return Encoding.UTF8.GetString(ms.ToArray());
            }
        }
    }
    
}
