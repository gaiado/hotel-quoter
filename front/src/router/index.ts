import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';

import ChannelView from '../views/ChannelView.vue';
import QuoterlView from '../views/QuoterView.vue';

const routes: RouteRecordRaw[] = [
    {
        path: '/channels',
        name: 'Channels',
        component: ChannelView
    },
    {
        path: '/quoter',
        name: 'Quoter',
        component: QuoterlView
    },
    {
        path: '/',
        name: 'home',
        redirect: {
            path: '/quoter',
        }

    }
];

const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router;