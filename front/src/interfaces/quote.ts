interface Pax {
    age: number;
}

interface RoomCriteria {
    paxes: Pax[];
}
interface HotelRateFilter {
    nonRefundable: boolean;
}

export interface Quote {
    checkIn: string;
    checkOut: string;
    market: string;
    roomCriteria: RoomCriteria[];
    hotelIds: number[];
    hotelRateFilter: HotelRateFilter;
}

