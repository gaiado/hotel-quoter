export interface Hotel {
    query: string;
    placeTypes: string;
    from: number;
    size: number;
}