import axios from 'axios';
import { Rate } from '~/interfaces/rate';
import { QuoteResponse } from '../interfaces/quoteResponse';


const quotesApi = axios.create({
    baseURL: 'http://localhost:5294/api'
});

const getQuote = async (rates: Rate): Promise<QuoteResponse> => {
    const { data } = await quotesApi.post<QuoteResponse>('/Quotes/Hotels/Rates', rates);
    return data;
};

export {
    getQuote
};