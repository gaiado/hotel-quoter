import axios from 'axios';
import { Channel } from '../interfaces/channel';


const channelApi = axios.create({
    baseURL: 'http://localhost:5294/api'
});

const getChannels = async (): Promise<Channel[]> => {
    const { data } = await channelApi.get<Channel[]>('/Channels');
    return data;
}

const getActiveChannels = async (): Promise<Channel[]> => {
    const { data } = await channelApi.get<Channel[]>('/Channels/active');
    return data;
}

const addChannel = async (channel: Channel): Promise<boolean> => {
    const { data } = await channelApi.post<Channel>('/Channels', channel);
    return true;
}

const updateChannel = async (channel: Channel): Promise<Channel> => {
    const { data } = await channelApi.put<Channel>('/Channels/' + channel.id, channel);
    return data;
}

const deleteChannel = async (channel: number): Promise<boolean> => {
    const { data } = await channelApi.delete<[]>('/Channels/'+ channel);
    return true;
}
export {
    getChannels,
    addChannel,
    updateChannel,
    deleteChannel,
    getActiveChannels
};